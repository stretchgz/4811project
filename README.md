# README

Smart Home Applications

Allow users to check the Arduino sensor data in modern web browsers.

## Arduino

Using the following sensors/devices:

- Temperature Sensor
- Light Sensor (Photocell)
- LEDs
- Motor (Fan)
- Ultrasonic Sensor

## Raspberry Pi

Web server will read sensor data from Arduino via the serialport (USB port).
Server is running in real-time. Allow users to check the sensors
without refreshing the page.

Server language: Javascript

## Main file list

```
.
├── app.js
├── bower.json
├── config.js
├── views/
│   ├── includes/
│       ├── head.jade
│       ├── footer.jade
│   ├── index.jade
├── dist/
│   ├── bower_components/
│   ├── scripts.js
│   ├── socket.io-1.3.5.js
│   └── style.css
├── node_modules
│   ├── compression/
│   ├── express/
│   ├── express-handlebars/
│   ├── serialport/
│   ├── socket.io/
│   └── validator/
└── package.json
```

**More Info About Third Party Packages**

* Server package: `package.json`
* Front-end package: `bower.json`

## Front-end packages:

* font-awesome (font-icon)
* jQuery (Javascript framework)
* materialize (bootstrap/front-end framework)

## Server-side packages:

* Express (web framework)
* Express-session (required by passportjs)
* compression (optional - enable GZIP on server)
* jade (template engine)
* passport (authentication for Node.js)
* socket.io (real-time framework)
* validator (validation)
* serialport (access/write serialport) 

# Installation

1. Download the source code
2. Use the Arduino IDE open the program in the `arduino` folder, then verify and upload the program to the Arduino.
3. Install Node.js on Raspberry Pi. For detailed installation steps see http://node-arm.herokuapp.com/ (perfered version 0.10.38)
4. Copy the `raspberryPi` folder to Raspberry Pi.
5. Install server files:

```bash
# cd to the path of raspberryPi
# $ cd ~/some/path/raspberryPi

# install bower
$ sudo npm install -g bower

# install server's dependency packages
$ npm install

# install front-end packages
$ bower install

# start server
$ npm start
```

## Serial Port errors when starting server

Plugin the Arduino to the Raspberry Pi. Then restart the Raspberry Pi.
Run the following command in Raspberry Pi terminal to find the
Arduino's Serial Port, then edit the port at line 22 of `app.js`.

```bash
$ dmesg | grep tty
```

## Contributors

Wai Daat Tsang

Dopdyel Tseten

## LICENSE

[MIT](http://opensource.org/licenses/MIT)