'use strict';
// load 3rd party packages
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var compression = require('compression');
var session = require('express-session');
var path = require('path');
var validator = require('validator');
var passport = require('passport');
var DigestStrategy = require('passport-http').DigestStrategy;
// use my own `config.js` file
var config = require('./config');

// serial port modules
var serialport = require('serialport');
var SerialPort = serialport.SerialPort; // localize object constructor

// config serial port
// Get port ID by "dmesg | grep tty" command
var sp = new SerialPort('/dev/ttyACM0', {
  baudrate: 9600,
  parser: serialport.parsers.readline('\n')
});

// express settings
app.set('view engine', 'jade'); // use `jade` template engine
app.enable('trust proxy'); // trust first proxy
app.use(passport.initialize()); // initialize passport.js
app.use(compression()); // GZIP static files, make them smaller
// set public asset folder path
app.use(express.static(__dirname + '/dist', {
  'index': false,
  'etag': false
}));

// real time magic
io.on('connection', function(socket) {
  var arduinoSP;
  // serial data ready
  sp.on('data', function(data) {
    // verify serial data
    if (validator.isJSON(data)) {
      try {
        arduinoSP = JSON.parse(data); // convert serial data to JSON
      } catch (e) {
        console.log(e); // display error if any
      }

      // just in case Raspberry Pi that did not pick up the entire line
      // check if arduino is printing the complete string
      // compare the data object key 'status' to '1' 
      if (arduinoSP.status === 1) {
        socket.emit('new arduino', arduinoSP); // send serial data to browser
      }
    }
  });

  /**
   * Serial Port communication
   * User sends event from browser to web server,
   * then server sends serial data to arduino
   * 
   * Buffer converts string to byte
   * https://nodejs.org/api/buffer.html#buffer_buffer
   **/
  socket.on('toggle.lightSensor', function() {
    // send the byte of `w` to arduino
    sp.write(new Buffer('w'));
  });

  socket.on('toggle.lightSensorLed', function() {
    // send the byte of `q` to arduino
    sp.write(new Buffer('q'));
  });

  socket.on('toggle.fanMotor', function() {
    // send the byte of `e` to arduino
    sp.write(new Buffer('e'));
  });

});

// Use the DigestStrategy within Passport.
//   This strategy requires a `secret`function, which is used to look up the
//   use and the user's password known to both the client and server.  The
//   password is used to compute a hash, and authentication will fail if the
//   computed value does not match that of the request.  Also required is a
//   `validate` function, which can be used to validate nonces and other
//   authentication parameters contained in the request.
passport.use(new DigestStrategy({
    qop: 'auth'
  },
  function(username, done) {
    // Find the user by username.  If there is no user with the given username
    // set the user to `false` to indicate failure.  Otherwise, return the
    // user and user's password.
    findByUsername(username, function(err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      return done(null, user, user.password);
    })
  },
  function(params, done) {
    // asynchronous validation, for effect...
    process.nextTick(function() {
      // check nonces in params here, if desired
      return done(null, true);
    });
  }
));

// read user credentials from config file
var users = config.credentials;
// compare input username to the username in the config file
function findByUsername(username, fn) {
  for (var i = 0, len = users.length; i < len; i++) {
    var user = users[i];
    if (user.username === username) {
      return fn(null, user);
    }
  }
  return fn(null, null);
}

// serve index page
app.get('/',
  // authenticate user
  passport.authenticate('digest', {
    session: false
  }),
  function(req, res) {
    // send and render the index.jade to browser (see `views/index.jade`)
    res.render('index', {
      title: 'Smart Home'
    });
  }
);

// start server at config port
server.listen(config.server.port, function() {
  console.log('Listening on port %d', server.address().port);
});
