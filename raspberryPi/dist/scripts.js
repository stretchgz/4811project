/*global io,Chart*/
$(document).ready(function() {
  'use strict';
  var socket = io();
  var lightSensorReading = $('#lightSensorReading');
  var ledState = $('#lightLedState');
  var frontLedState = $('#frontLedState');
  var fan = $('#fan');
  var tempC = $('#tempC');
  var tempF = $('#tempF');
  var ultraCm = $('#ultraCm');
  var ultraInch = $('#ultraInch');
  // toggle switch
  var toggleMotor = $('#toggle-motor');
  var toggleLightSensor = $('#toggle-lightSensor');
  var toggleLightSensorLight = $('#toggle-lightSensor-light');

  // receive new event from server
  socket.on('new arduino', function(data) {
    // uncomment the line below to see data in console
    //console.log(data);

    //verify json data
    if (data.status === 1) {
      // update text
      lightSensorReading.text(
        data.light.sensorState === 1 ? data.light.reading : 'Off');
      ledState.text(data.light.outputState === 1 ? 'On' : 'Off');
      tempC.text(data.temp.tempC);
      tempF.text(data.temp.tempF);
      ultraCm.text(data.ultrasonic.cm);
      ultraInch.text(data.ultrasonic.inch);
      fan.text(data.temp.outputState === 1 ? 'on' : 'off');
      frontLedState.text(data.ultrasonic.outputState === 1 ? 'on' : 'off');

      // toggle state color
      if (data.ultrasonic.outputState === 1) {
        frontLedState.addClass('font_green');
        frontLedState.siblings('.fa').addClass('font_green');
      } else {
        frontLedState.removeClass('font_green');
        frontLedState.siblings('.fa').removeClass('font_green');
      }
      if (data.light.outputState === 1) {
        ledState.addClass('font_green');
        ledState.siblings('.fa').addClass('font_green');
      } else {
        ledState.removeClass('font_green');
        ledState.siblings('.fa').removeClass('font_green');
      }
      // make icon rotate
      if (data.temp.outputState === 1) {
        $('.fa-spinner').addClass('fa-spin');
        fan.addClass('font_green');
      } else {
        $('.fa-spinner').removeClass('fa-spin');
        fan.removeClass('font_green');
      }
    }
  });

  // toggle clicks
  toggleMotor.on('click', function() {
    console.log('toggleMotor clicked');
    // emit envent to server
    socket.emit('toggle.fanMotor');
  });

  toggleLightSensor.on('click', function() {
    console.log('toggle light sensor');
    socket.emit('toggle.lightSensor');
  });

  toggleLightSensorLight.on('click', function() {
    console.log('toggle light sensor light');
    socket.emit('toggle.lightSensorLed');
  });

  // connection / disconnection
  socket.on('disconnect', function() {
    console.error('Disconnect: Connection to server has been lost.');
  });
  socket.on('reconnect', function() {
    console.log('Reconnect: Connection to server has been established.');
  });

});
