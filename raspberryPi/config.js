// config file for the web server
// You can change server port number, and login credentials.
module.exports = {

  // Server
  server: {
    port: process.env.PORT || 8000, // set port number
    secret: 'random cool stuff'
  },
  // http auth credentials
  credentials: [{
    id: 1,
    username: 'admin', // login user name
    password: 'password' // login password
  }]
};
