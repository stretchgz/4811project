#include <Servo.h>
Servo myservo; // servo object

// Define pins usage
int pinLightSensor = 0;  // pin for light (photocell) sensor (analog0)
int pinLightSensorLed = 11;   // pin for light sensor LED (D11)
int pinFrontDoor = 4; // pin for ultrasonic light (D4)
int pingTrig = 8; // ultrasonic Trig pin (D8)
int pingEcho = 12; // ultrasonic echo pin (D12)
int pinFan = 3; // pin for servo motor

// Light sensor variables
int lightSensorReading = 0, // LED brightness, photocell
    lightSensorTimer = 0,
    lightThreshold = 600; // set light sensor threshold

// ultrasonic sensor variables
float cm = 0,
      inch = 0,
      previousDistance = 0,
      currentDistance = 0,
      pingDuration = 0;
int   frontDoorLedTimer = 0;

// temperature
const int numReadings = 50; // take avg of last 50 temps
int   tempTimer = 0, tempIndex = 0;
float tempC = 0,
      tempF = 0,
      tempThreshold = 75, // set temp threshold
      tempTotal = 0,
      tempAvg = 0,
      tempReadings[numReadings];

// light snesor condition
boolean stateLightSensor = true;
boolean stateLed = false;
boolean lightManualMode = false;

// temp sensor conditions
boolean fanManualMode = false;
boolean toggleFanSensor = true;
boolean stateFan = false;

// ultrasonic sensor conditions
boolean stateFrontDoorLed = false;

// counter
int printEvery10 = 5; // control serial.print frequency

// initialize pins for input/output
void setup() {
  Serial.begin(9600);  //Begin serial communcation
  myservo.attach(pinFan); // setup servo pin
  pinMode(pinLightSensorLed, OUTPUT );
  pinMode(pinFrontDoor, OUTPUT);
  pinMode(pingTrig, OUTPUT);
  pinMode(pingEcho,  INPUT);

  // init. temp reading arary to zeros
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    tempReadings[thisReading] = 0;
  }
}

/*
 * main loop
 ***************************/
void loop() {
  /*
   *  Ultrasonic Sensor
   **************************/
  // The HC-SR04 is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse
  digitalWrite(pingTrig,  LOW);
  delayMicroseconds( 5);
  digitalWrite(pingTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingTrig,  LOW);
  pingDuration = pulseIn(pingEcho, HIGH);
  // unit conversion
  cm = microsecondsToCentimeters(pingDuration);
  inch = microsecondsToInches(pingDuration);
  currentDistance = inch;

  // if object is less than 10 inches away
  if (currentDistance < 10) {
    digitalWrite(pinFrontDoor, HIGH); // turn on front door LED
    stateFrontDoorLed = true;
    // when motor is on, the delay time changes, so reduce it accordingly
    if (stateFan == true) {
      frontDoorLedTimer = 4; // 5 sec
    } else {
      frontDoorLedTimer = 300;
    }
  } else {
    previousDistance = inch; // set last known distance to a variable
    frontDoorLedTimer--; // timer decrement by 1
    if (frontDoorLedTimer < 1 ) {
      digitalWrite(pinFrontDoor, LOW); // turn off front door LED
      stateFrontDoorLed = false;
    }
  }

  /*
   *  Temperature Sensor
   **************************/
  //Thermistor
  tempC = Thermistor(analogRead(A1), 1); // get current temp in celcius
  tempF = Thermistor(analogRead(A1), 2); // get current temp in fahrenheit

  // find the average temperature
  tempTotal = tempTotal - tempReadings[tempIndex];
  tempReadings[tempIndex] = tempF;
  tempTotal += tempReadings[tempIndex];
  tempIndex++;
  if (tempIndex >= numReadings) {
    tempIndex = 0; // reset when reach end of array
  }
  tempAvg = tempTotal / numReadings;

  // if room temp reach threshold
  if ( fanManualMode == 1 ) {
    // if manual mode on
    tempTimer = 300;
    stateFan = true;
    servoOn(); // turn on servo motor
  } else if (tempAvg > tempThreshold) {
    stateFan = true;
    tempTimer = 300; // 5 sec
    if (tempTimer > 0 ) {
      servoOn(); // turn on servo motor
    }
  } else {
    if (tempTimer > 0 ) {
      tempTimer--; // timer decrement by one
    }
    stateFan = false;
    if (fanManualMode == 0 && tempTimer == 0) {
      myservo.write(90);
    }
  }


  /*
   *  Light Sensor
   *************************/
  // Light Sensor Reading
  lightSensorReading = analogRead(pinLightSensor);
  // LED in pair with light sensor
  if ((lightSensorReading < lightThreshold && stateLightSensor == 1) ||
      (lightManualMode == 1 && stateLightSensor == 0)) {
    digitalWrite(pinLightSensorLed, HIGH);    // turn on LED
    stateLed = true;

    // when motor is on, the delay time changes, so reduce it accordingly
    if (stateFan == true) {
      lightSensorTimer = 4;
    } else {
      lightSensorTimer = 300; // set timer to 5 sec
    }
    // manual mode
    if (lightManualMode == 1 && stateLightSensor == 0) {
      lightSensorTimer = 0; // dont set timer for manual mode
    }
  } else {
    // when timer hit zero
    if (lightSensorTimer == 0) {
      digitalWrite(pinLightSensorLed, LOW); // turn off LED
      stateLed = false;
    }
    lightSensorTimer--; // decrement by 1
  }

  /*
   *  User Input / Enables manual mode
   *************************/
  // receive data
  if (Serial.available() > 0) {
    int inByte = Serial.read(); // read incoming Byte
    switch (inByte) {
      case 'e':
        // control motor manually
        if (fanManualMode == 1) {
          // turn off manual mode
          fanManualMode = 0;
        } else {
          // turn manual mode on
          fanManualMode = 1;
        }
        break;
      case 'w':
        // toggle light sensor
        if (stateLightSensor == 0 ) {
          stateLightSensor = 1; // sensor on
          lightManualMode = 0; // manual mode off
        } else {
          stateLightSensor = 0;
        }
        break;
      case 'q':
        // toggle light sensor LED
        if (lightManualMode == 0 ) {
          lightManualMode = 1; // manual mode on
          stateLightSensor = 0; // light sensor off
        } else {
          lightManualMode = 0; // turn off LED
        }
        break;
    }
  }

  /*
   *  Serial Print, print every ~1sec, or if motor is turned on then just print
   *  printed strings are in JSON format, so it can be easily parsed with JavaScript
   *************************/
  if (stateFan == true || printEvery10 == 50 ) {
    // let web server know the line is completely printed
    Serial.print("{\"status\":1,");
    // print light sensor data
    Serial.print("\"light\":{\"sensorState\":" + String(stateLightSensor) +
                 ",\"reading\":" + String(lightSensorReading) +
                 ",\"manualMode\":" + String(lightManualMode) +
                 ",\"outputState\":" + String(stateLed) +
                 "},");
    // print temperature sensor data
    Serial.print("\"temp\":{\"tempC\":" + String(tempC) +
                 ",\"tempF\":" + String(tempF) +
                 ",\"tempAvg\":" + String(tempAvg) +
                 ",\"outputState\":" + String(stateFan) +
                 ",\"manualMode\":" + String(fanManualMode) +
                 "},");
    // print ultrasonic sensor data
    Serial.print("\"ultrasonic\":{\"cm\":" + String(cm) +
                 ",\"inch\":" + String(inch) +
                 ",\"previousDistance\":" + String(previousDistance) +
                 ",\"currentDistance\":" + String(currentDistance) +
                 ",\"outputState\":" + String(stateFrontDoorLed) +
                 "}");
    Serial.println("}");

    printEvery10 = 1; // reset serial print condition
  } else {
    printEvery10++;
  }

  delay(15); // wait 15ms
}

long microsecondsToInches(long microseconds) {
  // According to Parallax's datasheet for the Ping sensor, there are
  // 73.746 microseconds per inch (i.e. sound travels at 1130 feet per
  // second).  This gives the distance travelled by the ping, outbound
  // and return, so we divide by 2 to get the distance of the obstacle.
  // See: http://www.parallax.com/dl/docs/prod/acc/28015-PING-v1.3.pdf
  return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

// Thermistor formula,
// see: http://playground.arduino.cc/ComponentLib/Thermistor2
double Thermistor(int RawADC, int tempUnit) {
  double Temp;
  Temp = log(10000.0 * ((1024.0 / RawADC - 1)));
  //         =log(10000.0/(1024.0/RawADC-1)) // for pull-up configuration
  Temp = 1 / (0.001129148 + (0.000234125 + (0.0000000876741 * Temp * Temp )) * Temp );

  switch (tempUnit) {
    case 1:
      // Convert Kelvin to Celcius
      Temp = Temp - 273.15;
      break;
    default:
      // Convert to Fahrenheit
      Temp = Temp * 9 / 5 - 459.67;
  }

  return Temp;
}

// Servo Motor takes about one second to complete the rotations (cw and ccw).
void servoOn() {
  int pos;
  for (pos = 0; pos < 180; pos += 1) // goes from 0 degrees to 180 degrees
  { // in steps of 1 degree
    myservo.write(pos); // tell servo to go to position in variable 'pos'
    delay(3);
  }
  for (pos = 180; pos >= 1; pos -= 1) // goes from 180 degrees to 0 degrees
  {
    myservo.write(pos); // tell servo to go to position in variable 'pos'
    delay(3);
  }
}

